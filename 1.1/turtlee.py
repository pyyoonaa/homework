import turtle as t
import math
turtle = t.Turtle()
t.shape('turtle')
screen=t.Screen()
t.color("purple")
t.pensize(1)
t.speed(0)
k=1
for i in range(1000):
    t.left(10*math.sin(k*2))
    t.fd(5)
    k+=0.01
screen.exitonclick()