import turtle
x = turtle.Turtle()
x.shape('turtle')
screen = turtle.Screen()
screen.setup(500,500)
screen.tracer(0)
x.color("purple")
x.speed(0)
x.width(3)
x.hideturtle()
def draw_square():
    for side in range(4):
        x.forward(100)
        x.left(90)
x.penup()
x.goto(-350, 0)
x.pendown()
while True :
    x.clear()
    draw_square()
    screen.update()
    x.forward(0.02)