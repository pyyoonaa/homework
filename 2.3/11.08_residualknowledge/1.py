import sys

a, b, c = input(), input(), input()
var1 = a
var2 = b
a = var2
b = c
c = var1
print(a, b, c)

def size(x, level=0):
    print("\t" * level, x.__class__, sys.getsizeof(x), x)
    if hasattr(x, '__iter__'):
        if hasattr(x, 'items'):
            for xx in x.items():
                show_sizeof(xx, level + 1)
        else:
            for xx in x:
                show_sizeof(xx, level + 1)