while True:
    try:
        n = int(input('Введите количество переменных: '))
        a = []
        for i in range(n):
            a.append(int(input(f'Введите переменную N{i + 1}: ')))
    except ValueError:
        print("Вы вводите не числа, попробуйте заново:")
        continue
    else:
        break
print(sum(a))

