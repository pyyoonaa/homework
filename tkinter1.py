import tkinter as tk

def func():
    try:
        x = float(entry_1.get())
        y = float(entry_2.get())
        label.config(text="Сумма: {}".format(x + y))
    except ValueError:
        label.config(text="Пожалуйста, ведите числа")

root = tk.Tk()
entry_1 = tk.Entry(root)
entry_2 = tk.Entry(root)
entry_1.pack()
entry_2.pack()
label = tk.Label(root, text="Сумма:")
label.pack()
button = tk.Button(root, text='Результат', command=func)
button.pack()
root.mainloop()