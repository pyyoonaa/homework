class Hex:
    r = 0
    area = 0

    def __init__(self, r):
        self.__r = r
        self.__area = ((3 ** 0.5) * 3 * r ** 2)/2

    def r(self):
        return self.__r
    def area(self):
        return self.__area

    def calc_area(self):
        self.__area = ((3 ** 0.5) * 3 * self.__r ** 2)/2

    def set_r(self, new_r):
        self.__r = new_r
        self.calc_area()

    def __str__(self):
        return str(self.__r)


print('Введите радиус описанной окружности: ')
x = Hex(float(input()))
print('Площадь: ', x.area())
