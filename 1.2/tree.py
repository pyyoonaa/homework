import pygame, sys
from pygame.locals import *

FPS = 30
rectbefore = (139, 69, 19)
elfbefore = (0, 100, 0)
elsbefore = (50, 205, 50)
eltbefore = (0, 128, 0)
rectafter = (128, 0, 0)
elfafter = (255, 215, 0)
elsafter = (255, 69, 0)
eltafter = (220, 20, 60)
boardWidth = 600
boardHeight = 600
myRectangle = pygame.Rect(210, 280, 50, 250)

def main():
    global FPSCLOCK, window
    pygame.init()
    FPSCLOCK = pygame.time.Clock()
    window = pygame.display.set_mode((boardWidth, boardHeight))

    mousex = 0
    mousey = 0

    button_color = rectbefore
    first_color = elfbefore
    sec_color = elsbefore
    thr_color = eltbefore
    mouseOver = False

    while True:
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYUP and event.key == K_ESCAPE):
                pygame.quit()
                sys.exit()
            elif event.type == MOUSEMOTION:
                mousex, mousey = event.pos
            elif event.type == MOUSEBUTTONUP:
                if mouseOver:
                    button_color = rectafter
                    first_color = elfafter
                    sec_color = elsafter
                    thr_color = eltafter

        mouseOver = determine_mouseOver(mousex, mousey)

        window.fill((60, 179, 113))
 
        pygame.draw.rect(window, button_color, myRectangle)
        pygame.draw.ellipse(window, first_color, (120, 200, 140, 160))
        pygame.draw.ellipse(window, sec_color, (160, 120, 140, 160))
        pygame.draw.ellipse(window, thr_color, (200, 220, 140, 160))

        if mouseOver:
            pygame.draw.rect(window, rectafter, myRectangle)
            pygame.draw.ellipse(window, elfafter, (120, 200, 140, 160))
            pygame.draw.ellipse(window, elsafter, (160, 120, 140, 160))
            pygame.draw.ellipse(window, eltafter, (200, 220, 140, 160))

        pygame.display.update()
        FPSCLOCK.tick(30)

def determine_mouseOver(valx, valy):
    if myRectangle.collidepoint(valx, valy):
        return True
    else:
        return False

main()
from sys import exit
while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
