import pygame, sys
from car import Car
from random import randint

pygame.init()
pygame.time.set_timer(pygame.USEREVENT, 500)

BLACK = (0, 0, 0)
W, H = 620, 570

sc = pygame.display.set_mode((W, H))

clock = pygame.time.Clock()
FPS = 60

font_end = pygame.font.SysFont('Times New Roman', 70)

mashina = pygame.image.load('images/main.png').convert_alpha()
m_rect = mashina.get_rect(centerx=W//2, bottom=H-5)

cars_images = ['carr.png', 'carra.png']

cars_surf = [pygame.image.load('images/'+path).convert_alpha() for path in cars_images]

def createCar(group):
    indx = randint(0, len(cars_surf)-1)
    x = randint(20, W-20)
    speed = 4

    return Car(x, speed, cars_surf[indx], group)

def collideCars():
    for car in cars:
        if m_rect.collidepoint(car.rect.center):
            while True:
                running = False
                render_end = font_end.render('Game over', 1, pygame.Color(255,200,0))
                sc.blit(render_end, (W // 2 - 200, W // 3))
                pygame.display.flip()
                from sys import exit
                while True:
                        for event in pygame.event.get():
                            if event.type == pygame.QUIT:
                                pygame.quit()
                                exit()


cars = pygame.sprite.Group()

bg = pygame.image.load('images/road.jpg').convert()

speed = 10

createCar(cars)
 
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()
        elif event.type == pygame.USEREVENT:
            createCar(cars)

    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        m_rect.x -= speed
        if m_rect.x < 0:
            m_rect.x = 0
    elif keys[pygame.K_RIGHT]:
        m_rect.x += speed
        if m_rect.x > W-m_rect.width:
            m_rect.x = W-m_rect.width
    
    collideCars()
    
    sc.blit(bg, (0, 0))
    cars.draw(sc)
    sc.blit(mashina, m_rect)
    pygame.display.update()

    clock.tick(FPS)

    cars.update(H)


