def pairs(op, clos):
    if op == '(' and clos == ')':
        return True
    if op == '[' and clos == ']':
        return True
    if op == '{' and clos == '}':
        return True
    return False

def check(s):
    stack = []

    for c in s:
        if c == '{' or c == '(' or c == '[':
            stack.append(c)
        elif c == '}' or c == ')' or c == ']':
            if len(stack) == 0:
                return False
            top_element = stack.pop()
            if not pairs(top_element, c):
                return False
    if len(stack) != 0:
        return False
    return True

print("Введите скобочную последовательность: ")
print(check(input()))