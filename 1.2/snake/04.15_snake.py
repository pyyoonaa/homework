import pygame
from random import randrange

R = 800
Size = 40

x, y = randrange(Size, R - Size, Size), randrange(Size, R - Size, Size)
apple = randrange(Size, R - Size, Size), randrange(Size, R - Size, Size)
snake = [(x, y)]
dx, dy = 0, 0
length = 1
dirs = {'W': True, 'S': True, 'A': True, 'D': True}
score = 0
speed_count = 0
snake_speed = 15
fps = 60

pygame.init()
surface = pygame.display.set_mode([R, R])
pygame.display.set_caption('Змейка')
clock = pygame.time.Clock()
font_score = pygame.font.SysFont('Tymes New Roman', 26)
font_end = pygame.font.SysFont('Tymes New Roman', 100)
fon = pygame.image.load('fon.jpg').convert()
pygame.mixer.music.load("sounds/fon.mp3")
pygame.mixer.music.play(-1)

s = pygame.mixer.Sound("sounds/nyam.mp3")

def close_game():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()

while True:
    surface.blit(fon, (0, 0))
    [pygame.draw.rect(surface, pygame.Color('orange'), (i, j, Size - 1, Size - 1)) for i, j in snake]
    pygame.draw.rect(surface, pygame.Color('red'), (*apple, Size, Size))
    render_score = font_score.render(f'SCORE: {score}', 1, pygame.Color('#FF1493'))
    surface.blit(render_score, (10, 10))
    speed_count += 1
    if not speed_count % snake_speed:
	    x += dx * Size
	    y += dy * Size
	    snake.append((x, y))
	    snake = snake[-length:]

    if snake[-1] == apple:
        apple = randrange(Size, R - Size, Size), randrange(Size, R - Size, Size)
        s.play()
        length += 1
        score += 1
        snake_speed -= 1
        snake_speed = max(snake_speed, 10)

    if x < 0 or x > R - Size or y < 0 or y > R - Size or len(snake) != len(set(snake)):
        while True:
            render_end = font_end.render('Игра окончена >:)', 1, pygame.Color('#800000'))
            surface.blit(render_end, (R // 2 - 200, R // 3))
            pygame.display.flip()
            

    key = pygame.key.get_pressed()
    if key[pygame.K_w]:
        if dirs['W']:
            dx, dy = 0, -1
            dirs = {'W': True, 'S': False, 'A': True, 'D': True}
    elif key[pygame.K_s]:
        if dirs['S']:
            dx, dy = 0, 1
            dirs = {'W': False, 'S': True, 'A': True, 'D': True}
    elif key[pygame.K_a]:
        if dirs['A']:
            dx, dy = -1, 0
            dirs = {'W': True, 'S': True, 'A': True, 'D': False}
    elif key[pygame.K_d]:
        if dirs['D']:
            dx, dy = 1, 0
            dirs = {'W': True, 'S': True, 'A': False, 'D': True}
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            
    pygame.display.flip()
    clock.tick(fps)
    close_game()
