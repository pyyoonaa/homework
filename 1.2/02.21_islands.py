with open('islands.txt', 'r') as file:
    a = file.readlines()
a = [[int(n) for n in x.split()] for x in a]
print(a)
ep = 2

def search_group(ar, row, elem):
    if ar[row][elem] == 1:
        ar[row][elem] = ep
        if elem < len(ar[row]) - 1:
            search_group(ar, row, elem + 1)
        if row < len(ar) - 1:
            search_group(ar, row + 1, elem)
        if elem > 0:
            search_group(ar, row, elem - 1)
        if row > 0:
            search_group(ar, row - 1, elem)
    else:
        return

rec_a = [i.copy() for i in a]
gr_cntr = 0
for row in range(len(rec_a)):
    for elem in range(len(rec_a[row])):
        if rec_a[row][elem] == 1:
            gr_cntr += 1
            search_group(rec_a, row, elem)

print(gr_cntr)
