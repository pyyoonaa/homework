column = [2, 4, 2, 6, 4, 2, 1, 4, 5, 3]
def water(column):
    if not column:
        return 0
    w = 0
    a, b = 0, len(column) - 1
    amax, bmax = column[a], column[b]
    while a < b:
        if amax <= bmax:
            w += amax - column[a]
            a += 1
            amax = max(amax, column[a])
        else:
            w += bmax - column[b]
            b -= 1
            bmax = max(bmax, column[b])
    return w
print("Количество воды: ")
print(water(column))